from math import inf, nan # Needed to clean up some divide by zero issues.

import pandas as pd


# List of all the columns to pull from CurrentHolds.csv into the dataframe.
# Note: Adv Plus shared is consortium-owned + ALL Adv Plus shared including local.
CSV_COLUMNS = [
    'ReserveID',
    'Title', 
    'Creator',
    'Format',
    'Audience/Rating',
    'Street date',
    'Adv holds',
    'Cons ratio',
    'Adv ratio',
    'Adv suspended holds',
    '1st active hold',
    'Cons own',
    'Adv own',
    'Adv Plus shared',
    'Cons Adv Plus Shared'
]


# Columns from the CurrentHolds.csv file that contain date information.
DATE_COLUMNS = [
    'Street date',
    '1st active hold'
]


# Columns in the output data used to create the DataTables table.
OUTPUT_COLUMNS = [
    'Title',
    'Format',
    'Audience/Rating',
    'Street date',
    'Active Adv holds',
    'Active Adv ratio',
    'Cons ratio',
    'Hold age',
    'Owned by library?',
    'Owned by Cons?',
    'Raw link'
]


class Holdfast:
    """A class for parsing an OverDrive Marketplace CurrentHolds.csv file
    into a pandas dataframe, performing some data cleanup and rounding,
    and adding the following columns:
        Active Adv holds
        Active Adv ratio
        Hold age
        Owned by library?
        Owned by Cons?
        In cons?
        Raw link
    
    The class can provide the hold data as a dict or another CSV.
    """
    
    def __init__(self, csv_path):
        """Initializes the class from the CurrentHolds.csv and sets up the dataframe."""
        
        # Reads CSV into a dataframe.
        self.dataframe = pd.read_csv(csv_path, usecols=CSV_COLUMNS, parse_dates=DATE_COLUMNS, dtype={'ISBN': str})
        
        # Adds a column of active Advance holds.
        self.dataframe['Active Adv holds'] = self.dataframe['Adv holds'] - self.dataframe['Adv suspended holds']
        
        # Adds a column for Active Adv ratios rounded to nearest tenth.
        self.dataframe['Active Adv ratio'] = self.dataframe['Active Adv holds'] / self.dataframe['Adv own']
        self.dataframe['Active Adv ratio'] = self.dataframe['Active Adv ratio'].round(1)
        self.dataframe['Active Adv ratio'] = self.dataframe['Active Adv ratio'].replace([inf, nan, 'NaN'], -1)
        
        # Rounds the Consortium ratios to nearest tenth.
        self.dataframe['Cons ratio'] = self.dataframe['Cons ratio'].round(1)
        self.dataframe['Cons ratio'] = self.dataframe['Cons ratio'].replace([inf, nan, 'NaN'], -1)
        
        # Adds a column of hold ages counted in days since 1st active hold.
        self.dataframe['Hold age'] = (pd.Timestamp('today') - self.dataframe['1st active hold']).dt.days
        
        # Adds a column that is True if local library owns a copy.
        self.dataframe['Owned by library?'] = self.dataframe['Adv own'].gt(0)
        
        # Adds a column that is True if there are Cons-level copies.
        self.dataframe['Owned by Cons?'] = self.dataframe['Cons own'].gt(0)
        
        # Adds a raw Marketplace link. Surprisingly, concatenating with + is extremely fast here.
        self.dataframe['Raw link'] = 'https://marketplace.overdrive.com/TitleDetails?crid=' + self.dataframe['ReserveID']

        # Converts "Young Adult" to "YA" for brevity.
        # Eventually this column will be broken into age and fic/nonfic columns.
        self.dataframe['Audience/Rating'] = self.dataframe['Audience/Rating'].str.replace('Young Adult', 'YA')

        # Initializes an output variable with all the chosen 
        # columns, plus weeds out all titles with 0 active holds.
        self.output = self.dataframe[OUTPUT_COLUMNS][self.dataframe['Active Adv holds'].gt(0)]

    def __str__(self) -> str:
        """Returns a string of the output, mostly for my debugging purposes."""
        return str(self.output)

    def to_dict(self):
        """Returns a dict primarily used by the web app."""
        return self.output.to_dict(orient='records')
    
    def to_csv(self):
        """Returns a CSV for further data manipulation elsewhere."""
        return self.output.to_csv(index=False)