# Holdfast

Sort and filter your OverDrive holds fast for easy ordering!

## Introduction

This is the Python script behind my Holdfast web app, which you can find [on my personal website](https://www.camorosi.com/holdfast). I wrote the script primarily to use behind the site, but I've also included a basic `to_csv()` method for standalone use.

### What Does It Do?

The Current Holds report is useful for putting together OverDrive Advantage orders that target what your patrons want, but it contains a dizzying amount of information. Much of the data isn't useful or needs some manipulation to become useful. Holdfast converts the raw report into much more useful data. Please refer to [the site](https://www.camorosi.com/holdfast) for a fuller explanation or to try out the web version yourself. It's quite simple!

### Dependencies

The only dependency is the `pandas` library.

`pip install pandas`

I am on Python 3.9.2.

## How To Use

1. Create a directory and then make a [virtual environment](https://docs.python.org/3.9/tutorial/venv.html) inside it.
2. Install `pandas`.
3. Download the `holdfast.py` script into your directory and, optionally, the `test_data.csv` file if you don't want to use your own OverDrive hold data.
4. With your venv active, write something like this and run it:
```python
from holdfast import Holdfast

hf = Holdfast('test_data.csv')

with open('output.csv', 'w') as outfile:
    outfile.write(hf.as_csv())
```
